# Création d'une instance
provider "aws" {
  region = "eu-west-1"

}
resource "aws_subnet" "main" {
  availability_zone = local.availability_zone
  vpc_id            = data.aws_vpc.my_vpc.id
  cidr_block        = var.subnet_cidr_block



  tags = {
    Name      = "genot.martin"
    Formation = "terraform"
    TP        = "TP1"
  }
}

resource "aws_route_table_association" "my_route_table_association" {
  subnet_id      = aws_subnet.main.id
  route_table_id = "rtb-0418a191a893f8d1f"
}

resource "aws_security_group" "my_security_group" {
  name_prefix = "genot.martin"
  vpc_id      = data.aws_vpc.my_vpc.id


  tags = {
    Name = "genot.martin"
    User = "genot.martin"
    TP   = var.tp
  }
}
resource "aws_security_group_rule" "my_security_group_rule_out_http" {
  type              = "egress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.my_security_group.id
}

resource "aws_security_group_rule" "my_security_group_rule_in_http" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.my_security_group.id
}

resource "aws_instance" "web" {
  ami                         = "ami-02297540444991cc0"
  subnet_id                   = aws_subnet.main.id
  instance_type               = "t2.micro"
  vpc_security_group_ids      = [aws_security_group.my_security_group.id]
  associate_public_ip_address = true
  tags = {
    "Name" = "genot.martin"
    "User" = "genot.martin"
    "TP"   = "TP2"
  }
}

output "instance_id" {
  description = "My aws instance id"
  value       = aws_instance.web.id
}

output "instance_public_ip" {
  description = "my aws instance public address ip"
  value       = aws_instance.web.public_ip
}